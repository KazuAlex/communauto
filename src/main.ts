import { createApp } from 'vue';
import { createPinia } from 'pinia';

import { Quasar } from 'quasar';
import quasarIconSet from 'quasar/icon-set/svg-fontawesome-v6';

import App from './App.vue';
import router from './router';
import i18n from './i18n';

import 'quasar/dist/quasar.css';
import 'leaflet/dist/leaflet.css';
import './assets/main.scss';

const app = createApp(App)
  .use(createPinia())
  .use(router)
  .use(i18n)
  .use(Quasar, {
    plugins: {},
    iconSet: quasarIconSet,
  })
  .mount('#app');
