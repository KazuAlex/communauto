import { createI18n } from 'vue-i18n';

import fr from './fr.json';
import en from './en.json';

export default createI18n({
  legacy: false,
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
    en,
    fr,
  },
});
