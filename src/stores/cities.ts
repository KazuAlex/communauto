import { defineStore } from 'pinia';
import type { Cities, City } from '@/types/cities';
import type {
  Accessory,
  VehicleBodyType,
  VehiclePropulsionType,
  VehicleType,
  VehicleTransmissionType,
  VehicleTireType,
} from '@/types/vehicles';
import { getCities, getGroupedAvailableInfo } from '@/api/cities';

export const useCitiesStore = defineStore({
  id: 'cities',
  state: () => ({
    cities: null as (City[] | null),
    selectedCity: null as (City | null),
    accessories: [] as Accessory[],
    bodyTypes: [] as VehicleBodyType[],
    propulsionTypes: [] as VehiclePropulsionType[],
    vehicleTypes: [] as VehicleType[],
    transmissionTypes: [] as VehicleTransmissionType[],
    tireTypes: [] as VehicleTireType[],
  }),
  getters: {
    getAccessory: (state) => {
      return (accessoryId: number) => {
        return state.accessories.find((a) => a.accessoryId === accessoryId);
      };
    },
    getAccessories: (state) => {
      return (accessoriesId: number[]) => {
        return state.accessories.filter((a) => accessoriesId.includes(a.accessoryId));
      };
    },
    getBodyType: (state) => {
      return (bodyTypeId: number) => {
        return state.bodyTypes.find((e) => e.vehicleBodyTypeId === bodyTypeId);
      };
    },
    getPropulsionType: (state) => {
      return (propulsionTypeId: number) => {
        return state.propulsionTypes.find((e) => e.vehiclePropulsionTypeId === propulsionTypeId);
      };
    },
    getVehicleType: (state) => {
      return (vehicleTypeId: number) => {
        return state.vehicleTypes.find((e) => e.vehicleTypeId === vehicleTypeId);
      };
    },
    getTransmissionType: (state) => {
      return (transmissionTypeId: number) => {
        return state.transmissionTypes.find((e) => e.vehicleTransmissionTypeId === transmissionTypeId);
      };
    },
    getTireType: (state) => {
      return (tireTypeId: number) => {
        return state.tireTypes.find((e) => e.vehicleTireTypeId === tireTypeId);
      };
    },
  },
  actions: {
    async init() {
      const cities = await getCities();
      this.cities = cities;

      const selectedCity = parseInt(localStorage.getItem('selectedCity'));
      if (selectedCity) {
        this.selectedCity = this.cities.find((city) => city.cityId === selectedCity);

        this.fetchGroupedAvailableInfos(this.selectedCity.cityId);
      }
    },

    async fetchGroupedAvailableInfos(cityId: number) {
      const groupedInfo = await getGroupedAvailableInfo(this.selectedCity.cityId);
      this.accessories = groupedInfo.vehicleAccessoriesList.accessories;
      this.bodyTypes = groupedInfo.vehicleTraitsList.vehicleBodyTypes;
      this.propulsionTypes = groupedInfo.vehicleTraitsList.vehiclePropulsionTypes;
      this.vehicleTypes = groupedInfo.vehicleTraitsList.vehicleTypes;
      this.transmissionTypes = groupedInfo.vehicleTraitsList.vehicleTransmissionTypes;
      this.tireTypes = groupedInfo.vehicleTraitsList.vehicleTireTypes;
    },

    async selectCity(city: City) {
      this.selectedCity = city;
      localStorage.setItem('selectedCity', city.cityId)
      await this.fetchGroupedAvailableInfos(this.selectedCity.cityId);
    },
  },
});
