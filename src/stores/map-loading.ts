import { defineStore } from 'pinia';

export const useMapLoadingStore = defineStore({
  id: 'mapLoading',
  state: () => ({
    stationsIsLoading: false,
    flexVehiclesIsLoading: false,
  }),
  getters: {
  },
  actions: {
  },
});
