import { defineStore } from 'pinia';
import dayjs from 'dayjs';

import { VehicleFilterType } from '@/types/filters';

const allowedMinutes = [0, 15, 30, 45];
const DATE_FORMAT = 'DD/MM/YYYY HH:mm';

export const useFiltersStore = defineStore({
  id: 'filter',
  state: () => ({
    type: VehicleFilterType.Any as VehicleFilterType,
    startDate: (() => {
      let date = dayjs().add(1, 'hour');
      let minute = date.minute();
      console.log('initStartDate', !allowedMinutes.includes(minute))
      if (!allowedMinutes.includes(minute)) {
        minute = allowedMinutes.find((m: number) => m > minute) || 0;
        date = date.minute(minute);
        console.log('initStartDate', minute, date.minute())
      }
      return date.format(DATE_FORMAT);
    })() as string,
    endDate: (() => {
      let date = dayjs().add(1, 'hour').add(1, 'day');
      let minute = date.minute();
      if (!allowedMinutes.includes(minute)) {
        minute = allowedMinutes.find((m: number) => m > minute) || 0;
        date = date.minute(minute);
      }
      return date.format(DATE_FORMAT);
    })() as string,
  }),
  getters: {
  },
  actions: {
  },
});
