import { defineStore } from 'pinia';
import type { Station } from '@/types/stations';
import { getStations } from '@/api/stations';

export const useStationsStore = defineStore({
  id: 'stations',
  state: () => ({
    stations: null as (Station[] | null),
  }),
  getters: {
    nbStations: (state) => {
      return state.stations ? state.stations.length : 0;
    },
    markers: (state) => {
      return state.stations
        ? state.stations.map((station: Station) => ({
          id: station.stationId,
          latLng: [station.stationLocation.latitude, station.stationLocation.longitude],
          station: station,
          type: 'station',
        })) : [];
    },
  },
  actions: {
    async fetchStations(cityId: number, startDate: number, endDate: number) {
      const stations = await getStations(cityId, startDate, endDate);
      this.stations = stations;
    },
  },
});
