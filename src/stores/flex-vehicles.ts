import { defineStore } from 'pinia';
import type { FlexVehicle } from '@/types/flex-vehicles';
import { getFlexVehicles } from '@/api/flex-vehicles';

export const useFlexVehiclesStore = defineStore({
  id: 'flexVehicles',
  state: () => ({
    flexVehicles: null as (FlexVehicle[] | null),
  }),
  getters: {
    nbVehicles: (state) => {
      return state.flexVehicles ? state.flexVehicles.length : 0;
    },
    markers: (state) => {
      return state.flexVehicles
        ? state.flexVehicles.map((veh: FlexVehicle) => ({
          id: veh.vehicleId,
          latLng: [veh.vehicleLocation.latitude, veh.vehicleLocation.longitude],
          flexVehicle: veh,
          type: 'flex',
        })) : [];
    },
  },
  actions: {
    async fetchFlexVehicles(cityId: number) {
      const flexVehicles = await getFlexVehicles(cityId);
      this.flexVehicles = flexVehicles;
    },
  },
});
