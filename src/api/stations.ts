import axios from 'axios';
import { FrontOfficeBaseUrl } from '@/constants/reservauto';
import type { Stations } from '@/types/stations';

const StationUrl = FrontOfficeBaseUrl + 'StationAvailability';
const StationVehicleDetailsUrl = FrontOfficeBaseUrl + 'Vehicle/{vehicleId}/StationBased';

type StationsResponse = {
  data?: Stations,
  status: number,
}

export const getStations: Promise<Stations> = (cityId: number, startDate: string, endDate: string) => {
  const bodyParams = [
    `cityId=${cityId}`,
    `startDate=${startDate}`,
    `endDate=${endDate}`,
    // `startDate=30/08/2022 12:00`,
    // `endDate=30/08/2022 18:00`,
  ].join('&');
  return axios.get(`${StationUrl}?${bodyParams}`)
    .then(({ data, status }: StationsResponse) => {
      if (status === 200) {
        return data.stations;
      }
      return null;
    });
}

export const getStationVehicleDetails: Promise<VehicleDetails> = (vehicleId: number, branchId: number) => {
  const uri = StationVehicleDetailsUrl.replace('{vehicleId}', vehicleId);
  return axios.get(`${uri}?branchId=${branchId}`)
    .then(({ data, status }: StationsResponse) => {
      if (status === 200) {
        return data;
      }
      return null;
    });
}
