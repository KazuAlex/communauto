import axios from 'axios';
import { FrontOfficeBaseUrl } from '@/constants/reservauto';
import type { FlexVehicle, FlexVehicles } from '@/types/flex-vehicles';
import type { VehicleDetails } from '@/types/vehicles';

const FlexVehiclesUrl = FrontOfficeBaseUrl + 'Vehicle/FreeFloatingAvailability';
const FlexVehicleDetailsUrl = FrontOfficeBaseUrl + 'Vehicle/{vehicleId}/FreeFloating';

type FlexVehiclesResponse = {
  data?: FlexVehicles,
  status: number,
}

export const getFlexVehicles: Promise<FlexVehicles[]> = (cityId: number) => {
  return axios.get(`${FlexVehiclesUrl}?cityId=${cityId}`)
    .then(({ data, status }: FlexVehiclesResponse) => {
      if (status === 200) {
        return data.vehicles;
      }
      return null;
    });
}

export const getFlexVehicleDetails: Promise<VehicleDetails> = (vehicleId: number, branchId: number) => {
  const uri = FlexVehicleDetailsUrl.replace('{vehicleId}', vehicleId);
  return axios.get(`${uri}?branchId=${branchId}`)
    .then(({ data, status }: FlexVehiclesResponse) => {
      if (status === 200) {
        return data;
      }
      return null;
    });
}
