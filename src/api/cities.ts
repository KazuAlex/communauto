import axios from 'axios';
import { FrontOfficeBaseUrl } from '@/constants/reservauto';
import type { Cities } from '@/types/cities';
import type { GroupedAvailableInfo } from '@/types/vehicles';

const BranchUrl = FrontOfficeBaseUrl + 'Branch/';
const BranchId = '1';
const CitiesUrl = BranchUrl + BranchId + '/AvailableCity';
const GroupedAvailableInfo = FrontOfficeBaseUrl + 'Vehicle/GroupedAvailableInfo';

type Response = {
  data?: Cities,
  status: number,
}

export const getCities: Promise<Cities> = () => {
 return axios.get(CitiesUrl)
   .then(({ data, status }: Response) => {
     if (status === 200) {
       return data.cities;
     } else {
       return null;
     }
   });
}

export const getGroupedAvailableInfo: Promise<GroupedAvailableInfo> = (cityId) => {
  return axios.get(`${GroupedAvailableInfo}?cityId=${cityId}`)
    .then(({ data, status }: Response) => {
      if (status === 200) {
        return data;
      } else {
        return null;
      }
    });
}
