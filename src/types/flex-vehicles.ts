export type FlexVehicle = {
  vehicleId: number,
  vehicleNb: number,
  cityId: number,
  vehiclePropulsionTypeId: number,
  vehicleTypeId: number,
  vehicleBodyTypeId: number,
  vehicleTransmissionTypeId: number,
  vehicleTireTypeId: number,
  vehiclePromotions: any[],
  vehicleLocation: {
    latitude: number,
    longitude: number,
  },
}

export type FlexVehicles = {
  totalNbVehicles: number,
  vehicles: FlexVehicle[],
}
