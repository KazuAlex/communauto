export type Station = {
  stationId: number,
  stationNb: number,
  stationName: string,
  cityId: number,
  stationLocation: {
    latitude: number,
    longitude: number,
  },
  recommendedVehicleId?: number,
  hasAllRequestedOptions: boolean,
  vehiclePromotions: number[],
  hasZone: boolean,
}

export type Stations = {
  stations: Station[],
}
