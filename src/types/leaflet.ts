import type { Station } from '@/types/stations';
import type { FlexVehicle } from '@/types/flex-vehicles';

export type Marker = {
  id: number,
  latLng: number[],
  station?: Station,
  flexVehicle?: FlexVehicle,
  type: any,
}

export type LeafletMarker = {
  id: number,
  type: string,
  station?: Station,
  flexVehicle?: FlexVehicle,
  instance?: any,
}
