type UsedAsFilterFor = {
  usedAsFilterFor: string[],
}

type LocalizedName = {
  localizedName: string,
}

export type VehicleDetails = {
  branchId: number,
  cityId: number,
  make: string,
  model: string,
  color: string,
  colorLocalizedName: string,
  nbDoors: number,
  nbPlaces: number,
  note: string,
  vehiclePlate: string,
  vehiclePropulsionTypeId: number,
  vehicleTypeId: number,
  vehicleBodyTypeId: number,
  vehicleTransmissionTypeId: number,
  vehicleTireTypeId: number,
  vehicleAccessories: number[],
  energyLevelPercentage: number,
  lastUsed: string,
  currentVehicleLocation: {
    latitude: number,
    longitude: number,
  },
  relocationPromotionZoneId: any | null,
  hasRentalNotifications: boolean,
  vehiclePromotions: any[],
  hasOnDemandFuelCreditCard: boolean,
  vehicleId: number,
  vehicleNb: number,
}

export type Accessory = {
  accessoryId: number,
  accessoryName: string,
  accessoryLocalizedName: string,
  accessoryAbbreviation: string,
} & UsedAsFilterFor;

export type VehicleBodyType = {
  vehicleBodyTypeId: number,
} & LocalizedName & UsedAsFilterFor;

export type VehiclePropulsionType = {
  vehiclePropulsionTypeId: number,
} & LocalizedName & UsedAsFilterFor;

export type VehicleType = {
  vehicleTypeId: number,
} & LocalizedName & UsedAsFilterFor;

export type VehicleTransmissionType = {
  vehicleTransmissionTypeId: number,
} & LocalizedName & UsedAsFilterFor;

export type VehicleTireType = {
  vehicleTireTypeId: number,
} & LocalizedName & UsedAsFilterFor;

export type Traits = {
  vehicleBodyTypes: VehicleBodyType[],
  vehiclePropulsionTypes: VehiclePropulsionType[],
  vehicleTypes: VehicleType[],
  vehicleTransmissionTypes: VehicleTransmissionType[],
  vehicleTireTypes: VehicleTireType[],
}

export type VehicleParkingType = {
  vehicleFreeFloatingParkingTypeId: number,
  parkingTypeName: string,
}

export type FreeFloatingParkingType = {
  parkingTypes: VehicleParkingType[],
}

export type GroupedAvailableInfo = {
  vehicleAccessoriesList: {
    accessories: Accessory[],
  },
  vehicleTraitsList: Traits,
  vehicleFreeFloatingParkingTypesList: FreeFloatingParkingType,
}
