export type City = {
  cityId: number,
  cityLocalizedName: string,
  branchId: number,
  isDefaultBranchCity: boolean,
  cityCenterLocation: {
    latitude: number,
    longitude: number,
  },
}

export type Cities = {
  cities: City[],
}
