export enum VehicleFilterType {
  Any = 'any',
  Flex = 'flex',
  Station = 'station',
}
