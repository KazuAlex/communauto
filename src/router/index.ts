import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import MapView from '@/views/MapView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/map',
      name: 'map',
      component: MapView,
    },
  ],
});

router.beforeEach((to, from) => {
  if (to.name !== 'home' && !localStorage.getItem('selectedCity')) {
    return { name: 'home' };
  }
});

export default router;
